from __future__ import division
from __future__ import print_function
import numpy as np

X = 0
Y = 1
RHO = 0
PHI = 1


def wrap2pi(angle):
    angle = (angle + np.pi) % (2 * np.pi) - np.pi
    return angle


def cart2pol(point_cart):
    # print('point cart', point_cart)
    rho = np.linalg.norm(point_cart, axis=1)
    phi = np.arctan2(point_cart[:, Y], point_cart[:, X])
    # print('rho phi', (rho, phi))
    return np.array((rho, phi))


def pol2cart(point_pol):
    # print('point pol', point_pol)
    x = point_pol[RHO] * np.cos(point_pol[PHI])
    y = point_pol[RHO] * np.sin(point_pol[PHI])
    xy = np.array((x, y))
    # print('xy', xy)
    return xy


if __name__ == '__main__':
    a = np.array((2, 2))
    a = [(0.0, 2.0)]
    a = np.array(a)
    b = [[9.59136260e+01], [-9.45577796e-02]]
    b = np.array(b)

    print(a.shape)
    # print('cart', a)
    # print('c2p', cart2pol(a))
    print('p2c', pol2cart(b))
    print('expected', [[ 95.48515523], [ -9.05587041]])