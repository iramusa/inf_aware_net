from __future__ import division
from __future__ import print_function

import simulation
import record
import target
import measures

import numpy as np
import random
import pickle
import time

N_TIMESTEPS = 10000
N_EMP = 3
ITERATIONS = 1
PERIOD = 1


def run_pure_physics():
    for i in range(ITERATIONS):
        print('iteration ', i)
        seed = i
        name = 'physics_' + str(seed)
        np.random.seed(seed)
        random.seed(seed)
        sim = simulation.Simulator(dt=0.1, pure_physics=True)
        sim.add_fixed_employees()
        rec = record.Record(seed, sim)

        for step in range(N_TIMESTEPS):
            sim.run_step()
            rec.add_timestep(sim)

        rec.save_file_name(name)


def analyse_files():
    FILE_IN_PREFIX = 'physics_'
    STRATEGIES = [
        # 'full_random',
        # 'in_fov',
        'inf_projection_bin',
        'inf_projection_gaus',
        'inf_gain_gaus',
        'in_fov_prop',
        # 'max_ent_decrease',
        # 'max_std_decrease',
    ]

    t_start = time.time()

    for strat in STRATEGIES:
        out_name = '{0}_{1}_{2}_{3}'.format(strat, PERIOD, N_EMP, ITERATIONS)
        results = Container(ITERATIONS, N_TIMESTEPS, N_EMP)
        for i in range(ITERATIONS):
            print('Time passed:', (time.time() - t_start)/60, 'minutes. Completed: ', i/ITERATIONS)

            file_in_name = FILE_IN_PREFIX + str(i)
            physics_run = record.load_file_name(file_in_name)
            new_rec = physics_run.add_tracking(strat, period=PERIOD)

            results.t = np.array([ts.t for ts in new_rec.timesteps])
            if len(new_rec.timesteps[0].emp_behaviours) != N_EMP:
                raise ValueError('Wrong number of employees!')

            for j, ts in enumerate(new_rec.timesteps):
                emp_pos = np.array(ts.emp_positions)
                for k in range(N_EMP):
                    true_pos = emp_pos[k]
                    true_beh = ts.emp_behaviours[k]
                    pos_p = ts.position_beliefs[k]
                    beh_p = ts.behaviour_beliefs[k]
                    w = ts.w[k]
                    p = w/np.sum(w)
                    results.true_pos[i, j, k, :] = true_pos
                    results.true_beh[i, j, k] = target.BEH2IDX[true_beh]
                    results.pos_close_perf[i, j, k] = measures.mass_close_pos_truth(true_pos, pos_p, p)
                    results.mean_dist_perf[i, j, k] = measures.mean_dist(true_pos, pos_p, p)
                    results.beh_close_perf[i, j, k] = measures.mass_beh_truth(true_beh, beh_p, p)
                    results.ent_bin_pre[i, j, k] = ts.entropies_bins_pre[k]
                    results.ent_bin_post[i, j, k] = ts.entropies_bins_post[k]
                    results.ent_gaus_pre[i, j, k] = ts.entropies_gaus_pre[k]
                    results.ent_gaus_post[i, j, k] = ts.entropies_gaus_post[k]

        results.save(out_name)


def add_tracking():
    name = 'physics_0'
    strat_type = 'in_fov'
    rec = record.load_file_name(name)
    new_rec = rec.add_tracking(strat_type)
    new_rec.replay()


class Container(object):
    def __init__(self, n_iter, n_ts, n_emp):
        self.t = np.zeros(n_ts)
        self.true_pos = np.zeros([n_iter, n_ts, n_emp, 2])
        self.true_beh = np.zeros([n_iter, n_ts, n_emp])
        self.pos_close_perf = np.zeros([n_iter, n_ts, n_emp])
        self.mean_dist_perf = np.zeros([n_iter, n_ts, n_emp])
        self.beh_close_perf = np.zeros([n_iter, n_ts, n_emp])
        self.ent_bin_pre = np.zeros([n_iter, n_ts, n_emp])
        self.ent_bin_post = np.zeros([n_iter, n_ts, n_emp])
        self.ent_gaus_pre = np.zeros([n_iter, n_ts, n_emp])
        self.ent_gaus_post = np.zeros([n_iter, n_ts, n_emp])

    def save(self, name):
        path = 'records/result_' + name + '.pkl'
        print('saving to ' + path)
        pickle.dump(self, open(path, 'wb'))


if __name__ == '__main__':
    run_pure_physics()
    analyse_files()


