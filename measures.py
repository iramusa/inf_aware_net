from __future__ import division
from __future__ import print_function

# self imports
from target import BEH2IDX

# imports
import numpy as np


# constants
DIST_THRESHOLD = 50


def mass_close_pos_truth(truth, particles, p):
    rel_pos = particles - truth
    dist = np.linalg.norm(rel_pos, axis=1)
    close = dist < DIST_THRESHOLD
    mass = np.sum(p[close])
    return mass


def mean_dist(truth, particles, p):
    rel_pos = particles - truth
    dist = np.linalg.norm(rel_pos, axis=1)
    av_dist = np.average(dist, weights=p)
    return av_dist


def mass_beh_truth(truth, beh_particles, p):
    correct = beh_particles == BEH2IDX[truth]
    mass = np.sum(p[correct])
    return mass


def entropy_2d_fixed_width(pos_p, weights, width=30):
    x, y = pos_p[:, 0], pos_p[:, 1]

    min_x = np.min(x)
    max_x = np.max(x)
    min_y = np.min(y)
    max_y = np.max(y)

    x_edges = np.ceil(1 + (max_x-min_x)/width)
    y_edges = np.ceil(1 + (max_y-min_y)/width)
#     print(x_edges, y_edges)

    # a = plt.hist2d(x, y, bins=(x_edges, y_edges))
    H, xedges, yedges = np.histogram2d(x, y, bins=(x_edges, y_edges), weights=weights)
    xwidth = xedges[1] - xedges[0]
    ywidth = yedges[1] - yedges[0]
    bin_area = xwidth * ywidth
    H = H[H>0]
    p = H/sum(H)
    ent = -sum(p*np.log2(p/bin_area))
    return ent
    # return np.maximum(ent, 0)


def gaussian_entropy_parts(pos_p, weights=None):
    if weights is None:
        weights = np.ones(len(pos_p))
    cov = np.cov(pos_p.T, aweights=weights)
    ent = 1 + np.log(2*np.pi) + np.log2(np.linalg.det(cov))/2
    # return ent
    return np.maximum(np.nan_to_num(ent), 8)


def gaussian_entropy(cov):
    ent = 1 + np.log(2*np.pi) + np.log2(np.linalg.det(cov))/2
    # return ent
    return np.maximum(np.nan_to_num(ent), 8)
