from __future__ import division
from __future__ import print_function

# imports
import numpy as np

# self imports
import target


def move_target(behaviour, cur_point, goal_point, dt):
    speed = target.BEH2SPEED[behaviour]
    if behaviour != target.B_TRAVERSING:
        # point in the random direction
        displacement = np.sqrt(dt) * speed * np.random.randn(2)

    else:
        noise = np.sqrt(dt) * target.BEH2NOISE[behaviour] * np.random.randn(2)
        displacement = move_towards(cur_point, goal_point, speed, dt) + noise

    # print('new_point', new_point)
    new_point = cur_point + displacement
    # print('dist covered', np.linalg.norm(displacement))

    return new_point


def move_towards(cur_point, goal_point, speed, dt):
    dir = goal_point - cur_point
    # avoid division by 0
    dist = np.linalg.norm(dir) + np.finfo(float).eps
    dir_norm = dir / dist
    # do not go farther than target
    displacement = min(speed * dt, dist) * dir_norm

    return displacement

    # old reliable version
# def move_towards(cur_point, goal_point, speed, dt):
#     dir = goal_point - cur_point
#     dist = np.linalg.norm(dir)
#     # norm = np.clip(np.linalg.norm(dir, axis=0), 0.001, 10000)
#     if dist != 0:
#         dir_norm = dir / dist
#         # do not go farther than target
#         displacement = min(speed * dt, dist) * dir_norm
#         # if np.linalg.norm(disp) > dist:
#         #     disp = dir
#         # print('normalistion', np.linalg.norm(dir_norm))
#     else:
#         displacement = np.zeros(2)
#
#     return displacement


def move_many_traversing(cur_pos, goal, speed, dt):
    rel_pos = goal - cur_pos
    dist = np.linalg.norm(rel_pos, axis=1) + np.finfo(float).eps
    dir_norm = rel_pos.T / dist
    displacement = np.minimum(speed*dt, dist) * dir_norm

    return displacement.T


    pass
