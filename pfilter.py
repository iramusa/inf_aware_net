from __future__ import division
from __future__ import print_function

# self imports
import scene as sc
import target as tg
import model
import measures

# imports
import numpy as np
import random
np.set_printoptions(precision=0, suppress=True)


# constants
X = 0
Y = 1
N_PARTICLES = 1000
INIT_SPREAD = 10
RESET_SPREAD = 200

# behaviour changes
ROOMS_OF_INTEREST = [sc.R_WAREHOUSE, sc.R_COMPUTER, sc.R_DINING]
WAREHOUSE_JOBS = np.array([tg.BEH2IDX[tg.B_WARE_WORK], tg.BEH2IDX[tg.B_REPAIR]])
# Probabilities of changes are per 1 second
P_TRAV2WORK = 0.4
P_WORK2TRAV = 0.03
P_WARE2REPAIR = 0.02
P_REPAIR2WARE = 0.02
P_GOAL_CHANGE = 0.9

NODE_RANGE = 40

SCALE_NOISE = 1
PF_BEH2NOISE = {
    tg.B_COMP_WORK: 3,
    tg.B_WARE_WORK: 100,
    tg.B_REPAIR: 10,
    tg.B_DINING: 3,
    tg.B_TRAVERSING: 20,
    tg.B_FROZEN: 1
}


class SingleTargetFilter(object):
    def __init__(self, building, name, color, initial_position=None, dummy=False):
        self.building = building
        self.target_name = name
        self.target_color = color
        self.n_particles = N_PARTICLES
        self.pos_p = np.zeros((self.n_particles, 2))
        self.beh_p = np.zeros(self.n_particles).astype(np.int8)
        self.w = np.ones(self.n_particles)
        self.w = self.w/np.sum(self.w)
        self.entropy_bin_pre = 0
        self.entropy_gaus_pre = 0
        self.entropy_bin_post = 0
        self.entropy_gaus_post = 0
        self.indices = np.array(range(self.n_particles))

        if initial_position is None:
            initial_position = sc.NODE_LOCATIONS['entrance']

        if dummy:
            np.zeros((self.n_particles, 2))
        else:
            self.pos_p = INIT_SPREAD * np.random.randn(self.n_particles, 2) + initial_position
            self.beh_p = np.repeat(tg.BEH2IDX[tg.B_TRAVERSING], self.n_particles)
            self.goal_p = np.zeros((self.n_particles, 2)) + sc.NODE_LOCATIONS['cor_war_a']

        self.entropy_bin_pre = measures.gaussian_entropy_parts(self.pos_p, self.w)
        self.entropy_bin_post = measures.gaussian_entropy_parts(self.pos_p, self.w)
        self.entropy_gaus_pre = measures.gaussian_entropy_parts(self.pos_p, self.w)
        self.entropy_gaus_post = measures.gaussian_entropy_parts(self.pos_p, self.w)

    def predict(self, dt):
        # change behaviour of some of the particles
        # this can be done from time to time
        self.predict_behaviours(dt)

        # for traversing parts
        self.predict_new_goals(dt)

        # motion
        self.move(dt)

        self.calculate_entropy_pre()

    def predict_behaviours(self, dt):
        for room in ROOMS_OF_INTEREST:
            in_room = self.building.is_point_in_room(self.pos_p, room)
            if not np.any(in_room):
                continue

            # change behaviour from traversing
            traversers = self.beh_p[in_room] == tg.BEH2IDX[tg.B_TRAVERSING]
            if np.any(traversers):
                p_change_trav2work = 1 - ((1 - P_TRAV2WORK)**dt)
                change_draw = np.random.rand(np.count_nonzero(traversers)) < p_change_trav2work
                relevant_indices = self.indices[in_room][traversers][change_draw]

                # two possible jobs only in warehouse
                if room == sc.R_WAREHOUSE:
                    n_changes = np.count_nonzero(change_draw)
                    job_draw = (2 * np.random.rand(n_changes)).astype(np.int8)
                    self.beh_p[relevant_indices] = WAREHOUSE_JOBS[job_draw]
                elif room == sc.R_DINING:
                    self.beh_p[relevant_indices] = tg.BEH2IDX[tg.B_DINING]
                elif room == sc.R_COMPUTER:
                    self.beh_p[relevant_indices] = tg.BEH2IDX[tg.B_COMP_WORK]

            # change behaviour of active workers
            non_traversers = np.logical_not(traversers)
            if np.any(non_traversers):
                p_change_work2trav = 1 - ((1 - P_WORK2TRAV)**dt)
                change_draw = np.random.rand(np.count_nonzero(non_traversers)) < p_change_work2trav
                relevant_indices = self.indices[in_room][non_traversers][change_draw]
                self.beh_p[relevant_indices] = tg.BEH2IDX[tg.B_TRAVERSING]

                goal_nodes = self.building.room2node[room]
                goal_locations = np.array([sc.NODE_LOCATIONS[node] for node in goal_nodes])
                draw_goals = (len(goal_nodes) * np.random.rand(len(relevant_indices))).astype(np.int8)
                new_goals = goal_locations[draw_goals]
                self.goal_p[relevant_indices] = new_goals

            if room == sc.R_WAREHOUSE:
                ware_workers = self.beh_p[in_room] == tg.BEH2IDX[tg.B_TRAVERSING]
                if np.any(ware_workers):
                    p_change_ware2repair = 1 - ((1 - P_WARE2REPAIR)**dt)
                    change_draw = np.random.rand(np.count_nonzero(ware_workers)) < p_change_ware2repair
                    relevant_indices = self.indices[in_room][ware_workers][change_draw]
                    self.beh_p[relevant_indices] = tg.BEH2IDX[tg.B_REPAIR]

                repairers = self.beh_p[in_room] == tg.BEH2IDX[tg.B_TRAVERSING]
                if np.any(repairers):
                    p_change_ware2repair = 1 - ((1 - P_REPAIR2WARE)**dt)
                    change_draw = np.random.rand(np.count_nonzero(repairers)) < p_change_ware2repair
                    relevant_indices = self.indices[in_room][repairers][change_draw]
                    self.beh_p[relevant_indices] = tg.BEH2IDX[tg.B_WARE_WORK]

    def predict_new_goals(self, dt):
        traversers = self.beh_p == tg.BEH2IDX[tg.B_TRAVERSING]
        p_change_goal = 1 - ((1 - P_GOAL_CHANGE)**dt)
        change_draw = np.random.rand(len(self.beh_p[traversers])) < p_change_goal

        pos_changers = self.pos_p[traversers][change_draw]
        left = np.ones(len(pos_changers[:, X])) > 0

        # loop until everyone is updated
        rooms = sc.ROOM_BORDERS.keys()
        # for room in rooms:
        while len(rooms) > 0 and np.any(left):
            # select random particle, use its room first
            sample_pos = random.choice(pos_changers[left])
            room = self.building.which_room(sample_pos)

            if room is None:
                room = rooms.pop()
            else:
                rooms.remove(room)

            in_room = self.building.is_point_in_room(pos_changers, room)  # not all points need to be checked here
            if not np.any(in_room):
                continue

            nodes = self.building.room2node[room]
            for node in nodes:
                node_location = sc.NODE_LOCATIONS[node]
                dists = np.linalg.norm(pos_changers[in_room] - node_location, axis=1)
                in_range = dists <= NODE_RANGE
                if not np.any(in_range):
                    continue

                goal_nodes = self.building.roadmap.neighbors(node)
                if len(goal_nodes) == 1:
                    new_goals = sc.NODE_LOCATIONS[goal_nodes[0]]
                else:
                    goal_locations = np.array([sc.NODE_LOCATIONS[node] for node in goal_nodes])
                    draw_goals = (len(goal_nodes) * np.random.rand(np.count_nonzero(in_range))).astype(np.int8)
                    new_goals = goal_locations[draw_goals]

                relevant_indices = self.indices[traversers][change_draw][in_room][in_range]
                self.goal_p[relevant_indices] = new_goals

            # update list of particles to change
            left[in_room] = False

    def move(self, dt):
        old_pos_p = np.copy(self.pos_p)
        for beh, idx in tg.BEH2IDX.items():
            workers = self.beh_p == idx
            noise = np.sqrt(dt) * SCALE_NOISE * PF_BEH2NOISE[beh] * np.random.randn(np.count_nonzero(workers), 2)

            if beh == tg.B_TRAVERSING:
                displacement = model.move_many_traversing(self.pos_p[workers], self.goal_p[workers],
                                                          1.2*tg.BEH2SPEED[tg.B_TRAVERSING], dt)
                self.pos_p[workers] += displacement + noise
            else:
                self.pos_p[workers] += noise

                # kill trespassers
                try:
                    room = tg.BEH2ROOM[beh]
                    in_room = self.building.is_point_in_room(self.pos_p[workers], room)
                    relevant_indices = self.indices[workers][np.logical_not(in_room)]
                    # WARNING: this is fairly arbitrary from the model's point of view
                    self.pos_p[relevant_indices] = old_pos_p[relevant_indices]

                    # another solution
                    # self.w[relevant_indices] = 0

                except KeyError:
                    pass

    def update_motion_detection(self, sensor):
        in_fov = sensor.is_in_fov(self.pos_p)
        if sensor.motion_detected:
            self.w[in_fov] *= sensor.p_d
            self.w[np.logical_not(in_fov)] *= 0
        else:
            self.w[in_fov] *= (1 - sensor.p_d)
            self.w[np.logical_not(in_fov)] *= 1

    def update_detection(self):
        pass

    def measurement_likelihood(self, dist_true, bear_true, polar_points, in_fov, sensor):
        like = np.ones(self.n_particles)

        if dist_true is not None:
            not_in_fov = np.logical_not(in_fov)

            like[not_in_fov] = 0

            dist_p, bear_p = polar_points

            dist_diff = dist_p - dist_true
            bear_diff = bear_p - bear_true

            dist_coef = 2 * np.pi*sensor.distance_error**2
            dist_like = np.exp(-dist_diff**2/dist_coef) * 1/np.sqrt(np.pi * dist_coef)
            bear_coef = 2 * np.pi*sensor.bearing_error**2
            bear_like = np.exp(-bear_diff**2/bear_coef) * 1/np.sqrt(np.pi * bear_coef)

            like[in_fov] *= dist_like * bear_like
        else:
            like[in_fov] = 1 - sensor.p_d

        like = like/np.sum(like)
        return like

    def update_id_detection(self, target_id, sensor):
        in_fov = sensor.is_in_fov(self.pos_p)

        if target_id in sensor.target_ids:
            det_idx = sensor.target_ids.tolist().index(target_id)
            # a = sensor.detections[:, target_id]
            dist_true, bear_true = sensor.detections[:, det_idx]

            self.w[in_fov] *= sensor.p_d
            self.w[np.logical_not(in_fov)] *= 0

            dist_p, bear_p = sensor.pos2reading(self.pos_p[in_fov])
            dist_diff = dist_p - dist_true
            bear_diff = bear_p - bear_true

            dist_coef = 2 * np.pi*sensor.distance_error**2
            dist_like = np.exp(-dist_diff**2/dist_coef) * 1/np.sqrt(np.pi * dist_coef)
            bear_coef = 2 * np.pi*sensor.bearing_error**2
            bear_like = np.exp(-bear_diff**2/bear_coef) * 1/np.sqrt(np.pi * bear_coef)

            self.w[in_fov] *= dist_like * bear_like

            if np.allclose(np.max(self.w), 0):
                detection_pos = sensor.reading2pos(dist_true, bear_true)
                self.pos_p = RESET_SPREAD * np.random.randn(self.n_particles, 2) + detection_pos
                self.beh_p = np.repeat(tg.BEH2IDX[tg.B_TRAVERSING], self.n_particles)
                nodes = self.building.room2node[sensor.room]
                node_choices = np.random.choice(nodes, self.n_particles)
                self.goal_p = np.array([sc.NODE_LOCATIONS[node] for node in node_choices])
                pass
        else:
            self.w[in_fov] *= (1 - sensor.p_d)

        if np.allclose(np.sum(self.w), 0):
            self.w = np.ones(self.n_particles)
        self.w /= np.sum(self.w)

    def update_beh_detection(self):
        pass

    def resample(self):
        if np.allclose(np.sum(self.w), 0):
            self.w = np.ones(self.n_particles)
            self.w /= np.sum(self.w)
            return

        # resample only if info was gained
        gain = self.entropy_bin_pre - self.entropy_bin_post
        if gain < 0.001:
            return

        self.w /= np.sum(self.w)
        new_samples = np.random.choice(self.indices, size=self.n_particles, p=self.w)

        self.pos_p = self.pos_p[new_samples]
        self.beh_p = self.beh_p[new_samples]
        self.goal_p = self.goal_p[new_samples]

        self.w = np.ones(self.n_particles)
        self.w /= np.sum(self.w)

    def calculate_entropy_pre(self):
        self.entropy_bin_pre = measures.entropy_2d_fixed_width(self.pos_p, self.w)
        self.entropy_bin_post = self.entropy_bin_pre
        self.entropy_gaus_pre = measures.gaussian_entropy_parts(self.pos_p, self.w)
        self.entropy_gaus_post = self.entropy_gaus_pre

    def calculate_entropy_post(self):
        self.entropy_bin_post = measures.entropy_2d_fixed_width(self.pos_p, self.w)
        self.entropy_gaus_post = measures.gaussian_entropy_parts(self.pos_p, self.w)

    def inf_gained_bin(self):
        return self.entropy_bin_pre - self.entropy_bin_post

    def inf_gained_gaus(self):
        return self.entropy_gaus_pre - self.entropy_gaus_post

    def calculate_exp_ig_gaus(self, sensor, n_measurement_samples=10):
        gain = 0

        SWITCH_THRESH = 0.02
        parts_in_fov = sensor.is_in_fov(self.pos_p)
        mass_in_fov = np.sum(self.w[parts_in_fov])
        mass_not_in_fov = 1 - mass_in_fov
        cur_ent = self.entropy_gaus_pre

        if mass_in_fov > 1.1:
            raise ValueError('mass in fov greater than 1: {0}'.format(mass_in_fov))

        # information gain in case of detection
        # hypothesis in fov is true
        if mass_in_fov > SWITCH_THRESH:
            aft_ent_samples = np.zeros(n_measurement_samples)

            polar_parts = np.array(sensor.pos2reading(self.pos_p[parts_in_fov]))
            indices = np.array(range(np.count_nonzero(parts_in_fov)))
            # measurement_indices = np.random.choice(self.indices[parts_in_fov], n_measurement_samples)
            measurement_indices = np.random.choice(indices, n_measurement_samples)

            for i, polar_m in enumerate(polar_parts[:, measurement_indices].T):
                dist_m, bear_m = polar_m
                dist_m += sensor.distance_error * np.random.randn()
                bear_m += sensor.bearing_error * np.random.randn()
                like = self.measurement_likelihood(dist_m, bear_m, polar_parts, parts_in_fov, sensor)
                aft_ent_samples[i] = measures.gaussian_entropy_parts(self.pos_p, self.w * like)

            # average over samples
            gain = sensor.p_d * mass_in_fov * (cur_ent - np.mean(aft_ent_samples))

        # information gain in case of no detection
        # hypothesis in fov [likely] untrue
        if mass_not_in_fov > SWITCH_THRESH and mass_in_fov < 1 - SWITCH_THRESH:
            like = np.ones(self.n_particles)
            like[parts_in_fov] = 1 - sensor.p_d
            like = like/np.sum(like)

            aft_ent = measures.gaussian_entropy_parts(self.pos_p, self.w * like)
            gain += mass_not_in_fov * sensor.p_d * (cur_ent - aft_ent)

        return gain

    def calculate_exp_ig_bin(self, sensor, n_measurement_samples=10):
        gain = 0

        SWITCH_THRESH = 0.02
        parts_in_fov = sensor.is_in_fov(self.pos_p)
        mass_in_fov = np.sum(self.w[parts_in_fov])
        mass_not_in_fov = 1 - mass_in_fov
        cur_ent = self.entropy_bin_pre

        if mass_in_fov > 1.1:
            raise ValueError('mass in fov greater than 1: {0}'.format(mass_in_fov))

        # information gain in case of detection
        # hypothesis in fov is true
        if mass_in_fov > SWITCH_THRESH:
            aft_ent_samples = np.zeros(n_measurement_samples)

            polar_parts = np.array(sensor.pos2reading(self.pos_p[parts_in_fov]))
            indices = np.array(range(np.count_nonzero(parts_in_fov)))
            # measurement_indices = np.random.choice(self.indices[parts_in_fov], n_measurement_samples)
            measurement_indices = np.random.choice(indices, n_measurement_samples)

            for i, polar_m in enumerate(polar_parts[:, measurement_indices].T):
                dist_m, bear_m = polar_m
                dist_m += sensor.distance_error * np.random.randn()
                bear_m += sensor.bearing_error * np.random.randn()
                like = self.measurement_likelihood(dist_m, bear_m, polar_parts, parts_in_fov, sensor)
                aft_ent_samples[i] = measures.entropy_2d_fixed_width(self.pos_p, self.w * like)

            # average over samples
            gain = sensor.p_d * mass_in_fov * (cur_ent - np.mean(aft_ent_samples))

        # information gain in case of no detection
        # hypothesis in fov [likely] untrue
        if SWITCH_THRESH < mass_not_in_fov < 1 - SWITCH_THRESH:
            like = np.ones(self.n_particles)
            like[parts_in_fov] = 1 - sensor.p_d
            like = like/np.sum(like)

            aft_ent = measures.entropy_2d_fixed_width(self.pos_p, self.w * like)
            gain += mass_not_in_fov * sensor.p_d * (cur_ent - aft_ent)

        return gain

    def calculate_inf_projection_gaus(self, sensor, projection_time=1, n_measurement_samples=10):
        SWITCH_THRESH = 0.02

        gain = 0
        # save current filter state
        pos_p_copy = np.copy(self.pos_p)

        parts_in_fov = sensor.is_in_fov(self.pos_p)
        mass_in_fov = np.sum(self.w[parts_in_fov])
        mass_not_in_fov = 1 - mass_in_fov
        cur_ent = self.entropy_gaus_pre

        if mass_in_fov > 1.1:
            raise ValueError('mass in fov greater than 1: {0}'.format(mass_in_fov))

        # information gain in case of detection
        # hypothesis in fov is true
        if mass_in_fov > SWITCH_THRESH:
            aft_ent_samples = np.zeros(n_measurement_samples)

            polar_parts = np.array(sensor.pos2reading(self.pos_p[parts_in_fov]))
            indices = np.array(range(np.count_nonzero(parts_in_fov)))
            # measurement_indices = np.random.choice(self.indices[parts_in_fov], n_measurement_samples)
            measurement_indices = np.random.choice(indices, n_measurement_samples)

            for i, polar_m in enumerate(polar_parts[:, measurement_indices].T):
                dist_m, bear_m = polar_m
                dist_m += sensor.distance_error * np.random.randn()
                bear_m += sensor.bearing_error * np.random.randn()
                like = self.measurement_likelihood(dist_m, bear_m, polar_parts, parts_in_fov, sensor)
                self.move(projection_time)
                aft_ent_samples[i] = measures.gaussian_entropy_parts(self.pos_p, self.w * like)
                self.pos_p = np.copy(pos_p_copy)

            # average over samples
            gain = sensor.p_d * mass_in_fov * (cur_ent - np.mean(aft_ent_samples))

        # information gain in case of no detection
        # hypothesis in fov [likely] untrue
        if mass_not_in_fov > SWITCH_THRESH and mass_in_fov < 1 - SWITCH_THRESH:
            like = np.ones(self.n_particles)
            like[parts_in_fov] = 1 - sensor.p_d
            like = like/np.sum(like)

            self.move(projection_time)
            aft_ent = measures.gaussian_entropy_parts(self.pos_p, self.w * like)
            gain += mass_not_in_fov * sensor.p_d * (cur_ent - aft_ent)
            self.pos_p = np.copy(pos_p_copy)

        return gain

    def calculate_inf_projection_bin(self, sensor, projection_time=1, n_measurement_samples=10):
        SWITCH_THRESH = 0.02

        gain = 0
        # save current filter state
        pos_p_copy = np.copy(self.pos_p)

        parts_in_fov = sensor.is_in_fov(self.pos_p)
        mass_in_fov = np.sum(self.w[parts_in_fov])
        mass_not_in_fov = 1 - mass_in_fov
        cur_ent = self.entropy_bin_pre

        if mass_in_fov > 1.1:
            raise ValueError('mass in fov greater than 1: {0}'.format(mass_in_fov))

        # information gain in case of detection
        # hypothesis in fov is true
        if mass_in_fov > SWITCH_THRESH:
            aft_ent_samples = np.zeros(n_measurement_samples)

            polar_parts = np.array(sensor.pos2reading(self.pos_p[parts_in_fov]))
            indices = np.array(range(np.count_nonzero(parts_in_fov)))
            measurement_indices = np.random.choice(indices, n_measurement_samples)

            for i, polar_m in enumerate(polar_parts[:, measurement_indices].T):
                dist_m, bear_m = polar_m
                dist_m += sensor.distance_error * np.random.randn()
                bear_m += sensor.bearing_error * np.random.randn()
                like = self.measurement_likelihood(dist_m, bear_m, polar_parts, parts_in_fov, sensor)
                self.move(projection_time)
                aft_ent_samples[i] = measures.entropy_2d_fixed_width(self.pos_p, self.w * like)
                self.pos_p = np.copy(pos_p_copy)

            # average over samples
            gain = sensor.p_d * mass_in_fov * (cur_ent - np.mean(aft_ent_samples))

        # information gain in case of no detection
        # hypothesis in fov [likely] untrue
        if mass_not_in_fov > SWITCH_THRESH and mass_in_fov < 1 - SWITCH_THRESH:
            like = np.ones(self.n_particles)
            like[parts_in_fov] = 1 - sensor.p_d
            like = like/np.sum(like)

            self.move(projection_time)
            aft_ent = measures.entropy_2d_fixed_width(self.pos_p, self.w * like)
            gain += mass_not_in_fov * sensor.p_d * (cur_ent - aft_ent)
            self.pos_p = np.copy(pos_p_copy)

        return gain


    def draw_particles(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        all_behs = self.beh_p >= 0
        # only = self.beh_p == tg.BEH2IDX[tg.B_WARE_WORK]
        # only = self.beh_p == tg.BEH2IDX[tg.B_COMP_WORK]
        only = all_behs

        points = np.copy(self.pos_p)
        points[only, Y] = self.building.y_max - points[only, Y]

        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        ax.scatter(points[only, X], points[only, Y], c=self.target_color, marker='.', s=1, edgecolor='face')

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

        return ax

    def get_mean_position(self):
        x, y = np.mean(self.pos_p, axis=0)
        return x, y

    def get_covar(self):
        return np.cov(self.pos_p.T)

    def get_beh(self):
        beh_likelihoods = {}
        for beh, idx in tg.BEH2IDX.items():
            likelihood = np.float(np.sum(self.beh_p == idx)/self.n_particles)
            beh_likelihoods.update({beh: likelihood})

        return beh_likelihoods

    def __str__(self):
        x, y = self.get_mean_position()
        pos = 'Mean position:\nx: {0:.0f} y: {1:.0f}\n'.format(x, y)
        # pos_var = 'Covariance in position:\n{0}\n'.format(self.get_covar())
        beh = 'Behaviour likelihoods:\n{0}'.format(self.get_beh())

        # return pos + pos_var + beh
        return pos + beh


