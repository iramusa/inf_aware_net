from __future__ import division
from __future__ import print_function

# self imports
import target
import sensor
import pfilter
import strategy
import simulation

# imports
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import pickle
import bisect
import random


def load_file(id):
    name = 'records/run_' + str(id) + '.pkl'
    rec = pickle.load(open(name))
    return rec


def load_file_name(name):
    name = 'records/' + name + '.pkl'
    rec = pickle.load(open(name))
    return rec


def replay_file(run_id):
    rec = load_file(run_id)
    rec.replay()


def record_clip(run_id, t_0, t_f):
    rec = load_file(run_id)
    rec.generate_clip(run_id, t_0, t_f)


class Record(object):
    def __init__(self, run_id, sim):
        # self.sim = copy.deepcopy(sim)
        self.id = run_id
        self.dt = sim.dt
        self.building = sim.building
        self.timesteps = []
        self.idx_offset = 0

        self.pure_physics = sim.pure_physics
        self.cameras = sim.cameras
        self.employees = sim.employees
        self.filters = sim.filters

    def add_timestep(self, sim):
        self.timesteps.append(TimeStep(sim))

    def save_file(self):
        name = 'records/run_' + str(self.id) + '.pkl'
        print('saving to ' + name)
        pickle.dump(self, open(name, 'wb'))

    def save_file_name(self, name):
        path = 'records/' + name + '.pkl'
        # print('saving to ' + path)
        pickle.dump(self, open(path, 'wb'))
        print('Complete ' + path)

    def replay(self):
        plt.ion()
        plt.figure(1)
        for step in self.timesteps:
            step.draw_step()
            plt.pause(0.01)

    def add_tracking(self, strat_type, period=1):
        if not self.pure_physics:
            raise ValueError('already has tracking!!')

        np.random.seed(self.id)
        random.seed(self.id)

        new_sim = simulation.Simulator(dt=self.dt, strategist_type=strat_type, pure_physics=False)
        new_sim.add_fixed_employees()

        strat = strategy.Strategist(strat_type, dt=period*self.dt)
        strat.add_sensors(new_sim.cameras)
        strat.add_all_filters(new_sim.filters)

        new_rec = Record(self.id, new_sim)

        for i, step in enumerate(self.timesteps):
            # print('t ', step.t)
            new_sim.t = step.t
            new_sim.camera_active = None
            for camera in new_sim.cameras:
                camera.clear_detections()

            for j, emp in enumerate(new_sim.employees):
                emp.behaviour = step.emp_behaviours[j]
                emp.position = step.emp_positions[j]

            perform_update = not (i % period)
            new_sim.run_filter(perform_update)
            new_rec.add_timestep(new_sim)

            # new_sim.draw_all()
            # plt.pause(0.2)

        return new_rec
        # name = strat_type + '_' + str(self.id)
        # new_rec.save_file_name(name)

    def draw_step_i(self, i):
        step = self.timesteps[self.idx_offset + i]
        step.draw_step()

    def generate_clip(self, run_id, t_start, t_end):
        times = [ts.t for ts in self.timesteps]
        idx_start = bisect.bisect_left(times, t_start)
        self.idx_offset = idx_start
        idx_end = bisect.bisect_right(times, t_end)
        n_iter = idx_end - idx_start

        fig = plt.figure(2)
        anim = animation.FuncAnimation(fig,
                                       self.draw_step_i,
                                       frames=n_iter,
                                       interval=1)

        anim.save('clips/run_{0}.avi'.format(run_id), fps=5, extra_args=['-vcodec', 'libx264'])
        # plt.show()


class TimeStep(object):
    def __init__(self, sim):
        self.t = sim.t
        self.pure_physics = sim.pure_physics
        self.building = sim.building
        self.sensors = sim.cameras
        self.sensor_active = sim.camera_active

        self.emp_names = []
        self.emp_colors = []
        self.emp_behaviours = []
        self.emp_positions = []

        self.sensor = None
        self.detections = []
        self.target_ids = []
        self.target_colors = []
        self.target_behaviours = []
        self.motion_detected = False

        self.position_beliefs = []
        self.behaviour_beliefs = []
        self.w = []

        self.entropies_bins_pre = []
        self.entropies_bins_post = []
        self.entropies_gaus_pre = []
        self.entropies_gaus_post = []

        self.add_targets(sim.employees)

        if not sim.pure_physics:
            if self.sensor_active is not None:
                self.add_sensor(sim.cameras[sim.camera_active])

            self.add_beliefs(sim.filters)

    def add_targets(self, targets):
        self.emp_names = [tg.name for tg in targets]
        self.emp_colors = [tg.color for tg in targets]
        self.emp_behaviours = [tg.behaviour for tg in targets]
        self.emp_positions = [tg.position for tg in targets]

    def add_sensor(self, sens):
        self.sensor = sens
        self.detections = np.copy(sens.detections)
        self.target_ids = np.copy(sens.target_ids)
        self.target_colors = np.copy(sens.target_colors)
        self.target_behaviours = np.copy(sens.target_behaviours)
        self.motion_detected = sens.motion_detected

    def add_beliefs(self, pfs):
        self.position_beliefs = [np.copy(pf.pos_p) for pf in pfs]
        self.behaviour_beliefs = [np.copy(pf.beh_p) for pf in pfs]
        self.w = [np.copy(pf.w) for pf in pfs]
        self.entropies_bins_pre = [pf.entropy_bin_pre for pf in pfs]
        self.entropies_bins_post = [pf.entropy_bin_post for pf in pfs]
        self.entropies_gaus_pre = [pf.entropy_gaus_pre for pf in pfs]
        self.entropies_gaus_post = [pf.entropy_gaus_post for pf in pfs]

    def draw_step(self):
        targets = []
        pfs = []
        for i in range(len(self.emp_colors)):
            tg = target.Target(self.building,
                               self.emp_names[i],
                               self.emp_colors[i],
                               dummy=True)
            tg.position = self.emp_positions[i]
            tg.behaviour = self.emp_behaviours[i]
            targets.append(tg)

            if not self.pure_physics:
                pf = pfilter.SingleTargetFilter(self.building,
                                                self.emp_names[i],
                                                self.emp_colors[i],
                                                dummy=True)
                pf.pos_p = self.position_beliefs[i]
                pf.beh_p = self.behaviour_beliefs[i]
                pfs.append(pf)
                # print(pf)

        ax = self.building.draw_map()
        for tg in targets:
            tg.draw_target(ax)

        for sens in self.sensors:
            sens.switch = False
            sens.draw_sensor(ax)

        if not self.pure_physics:
            for pf in pfs:
                pf.draw_particles(ax)

            if self.sensor_active is not None:
                self.sensor.switch = True
                self.sensor.motion_detected = self.motion_detected
                self.sensor.detections = self.detections
                self.sensor.target_ids = self.target_ids
                self.sensor.target_colors = self.target_colors
                self.sensor.target_behaviours = self.target_behaviours
                self.sensor.draw_sensor(ax)
                self.sensor.draw_detections(ax)

        self.draw_time(ax)

    def draw_time(self, ax):
        x = 30
        y = 60
        ax.text(x, y, str(self.t))


if __name__ == '__main__':
    replay_file(0)
    # record_clip(2, 0.6, 10.0)
