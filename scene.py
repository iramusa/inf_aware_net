from __future__ import division
from __future__ import print_function

# Contains information about the building such as:
# - map image
# - room names and locations
# - nodes used for locomotion
# Contains functions for:
# - checking given points locations (room, closest node)
# - generation of path between nodes

# imports
import cv2
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# constants
X = 0
Y = 1
LOW = 0
HIGH = 1

# rooms
R_WAREHOUSE = 'warehouse'
R_COMPUTER = 'computer'
R_CORRIDOR_A = 'corridor_a'
R_CORRIDOR_B = 'corridor_b'
R_DINING = 'dining'
# rooms borders
ROOM_BORDERS = {}
ROOM_BORDERS.update({R_CORRIDOR_A: ((430, 880), (20, 140))})
ROOM_BORDERS.update({R_CORRIDOR_B: ((300, 430), (20, 880))})
ROOM_BORDERS.update({R_DINING: ((20, 280), (410, 720))})
ROOM_BORDERS.update({R_COMPUTER: ((20, 280), (20, 380))})
ROOM_BORDERS.update({R_WAREHOUSE: ((450, 880), (160, 880))})
for room, borders in ROOM_BORDERS.items():
    ROOM_BORDERS.update({room: np.array(borders)})


# nodes
NODE_LOCATIONS = {
    'entrance': (860, 70),
    'mid_cor': (370, 70),
    'cor_war_a': (750, 70),
    'war_cor_a': (750, 200),
    'cor_comp': (340, 185),
    'comp_cor': (250, 185),
    'comp_a': (35, 30),
    'comp_b': (35, 360),
    'comp_c': (265, 360),
    'comp_d': (265, 30),
    'cor_din': (340, 510),
    'din_cor': (250, 510),
    'din_a': (35, 430),
    'din_b': (35, 700),
    'din_c': (265, 700),
    'din_d': (265, 430),
    'cor_war_b': (400, 810),
    'war_cor_b': (490, 810),
    'mid_war': (750, 500),
    'lef_war': (490, 190),
    'rig_war': (845, 830),
    'low_war': (845, 190),
}
for node, location in NODE_LOCATIONS.items():
    NODE_LOCATIONS.update({node: np.array(location)})

NODES = NODE_LOCATIONS.keys()

NODE_CONNECTIONS = {
    'entrance': [],
    'mid_cor': ['entrance'],
    'cor_war_a': ['entrance', 'mid_cor'],
    'war_cor_a': ['cor_war_a'],
    'cor_comp': ['mid_cor'],
    'comp_cor': ['cor_comp'],
    'comp_a': ['comp_cor'],
    'comp_b': ['comp_cor', 'comp_a'],
    'comp_c': ['comp_cor', 'comp_a', 'comp_b'],
    'comp_d': ['comp_cor', 'comp_a', 'comp_b', 'comp_c'],
    'cor_din': ['cor_comp'],
    'din_cor': ['cor_din'],
    'din_a': ['din_cor'],
    'din_b': ['din_cor', 'din_a'],
    'din_c': ['din_cor', 'din_a', 'din_b'],
    'din_d': ['din_cor', 'din_a', 'din_b', 'din_c'],
    'cor_war_b': ['cor_din', 'mid_cor'],
    'war_cor_b': ['cor_war_b'],
    'mid_war': ['war_cor_b', 'war_cor_a'],
    'lef_war': ['war_cor_b', 'war_cor_a', 'mid_war'],
    'rig_war': ['war_cor_b', 'mid_war'],
    'low_war': ['war_cor_a', 'mid_war', 'rig_war']
}

# construct a roadmap
ROADMAP = nx.Graph()
for node in NODES:
    for neigh in NODE_CONNECTIONS[node]:
        distance = np.linalg.norm(NODE_LOCATIONS[node] - NODE_LOCATIONS[neigh])
        ROADMAP.add_edge(node, neigh, weight=distance)


class Scene(object):
    def __init__(self):
        self.map = cv2.imread('pictures/map.png')
        self.x_max, self.y_max = self.map.shape[0:2]
        self.room_borders = ROOM_BORDERS
        self.rooms = self.room_borders.keys()
        self.node_locations = NODE_LOCATIONS
        self.node2room = {}
        self.room2node = {}
        for node, location in self.node_locations.items():
            room = self.which_room(location)
            self.node2room.update({node: room})

            if room not in self.room2node.keys():
                self.room2node.update({room: [node]})
            else:
                self.room2node[room].append(node)

        self.roadmap = ROADMAP

    def random_point_in(self, room):
        """Returns a random point in a given room

        :param room: room name
        :return: a random point in room
        """
        borders = self.room_borders[room]
        b_len = borders[:, HIGH] - borders[:, LOW]
        randomness = np.random.rand(2)
        point = np.array((borders[:, LOW] + b_len * randomness))
        return point

    def is_point_in_room(self, point, room):
        """Is this point in a given room?

        :param point: a single point or an array of n points
        :param room: a specific room
        :return: True or False for a single point input, array of n booleans for array input
        """
        point = np.array(point)
        border = self.room_borders[room]
        # print('point', point)
        x, y = point.T
        res = np.logical_and(np.logical_and(border[X, LOW] <= x, x <= border[X, HIGH]),
                             np.logical_and(border[Y, LOW] <= y, y <= border[Y, HIGH]))
        return res

    def which_room(self, point):
        """Which of the rooms does the point inhabits?

        :param point: single point
        :return: name of the room or None
        """
        for room in self.rooms:
            if self.is_point_in_room(point, room):
                return room

        # print('Warning point ', point, 'not in a room')
        return None

    def get_node_path(self, start, goal):
        """Returns shortest path between start and goal node.

        :param start: node
        :param goal: node
        :return: list of nodes
        """
        path = nx.dijkstra_path(self.roadmap, start, goal)
        return path

    def get_full_path(self, p0, pf):
        """Returns shortest valid path between two points in the building.

        :param p0: start point
        :param pf: end point
        :return: list of points to visit
        """
        start_node = 'start'
        end_node = 'end'
        start_room = self.which_room(p0)
        end_room = self.which_room(pf)

        for node in self.room2node[start_room]:
            distance = np.linalg.norm(p0 - self.node_locations[node])
            self.roadmap.add_edge(start_node, node, weight=distance)

        for node in self.room2node[end_room]:
            distance = np.linalg.norm(pf - self.node_locations[node])
            self.roadmap.add_edge(end_node, node, weight=distance)

        if start_room == end_room:
            distance = np.linalg.norm(pf - p0)
            self.roadmap.add_edge(end_node, start_node, weight=distance)

        path = self.get_node_path(start_node, end_node)
        self.roadmap.remove_node(start_node)
        self.roadmap.remove_node(end_node)
        points = [p0]
        for node in path[1:-1]:
            points.append(self.node_locations[node])
        points.append(pf)

        return points

    def draw_map(self, ax=None, plot_rooms=False):
        if ax is None:
            fig = plt.gcf()
            fig.clf()
            ax = plt.subplot(111)

        plt.imshow(self.map)

        # plot rooms
        if plot_rooms is True:
            for name, borders in self.room_borders.items():
                ax.text(borders[X][LOW], self.y_max - borders[Y][LOW], name)
                ax.add_patch(
                    patches.Rectangle(
                        (borders[X][LOW], self.y_max - borders[1][0]),
                        borders[X][HIGH]-borders[X][LOW],
                        borders[Y][LOW]-borders[Y][HIGH],
                        fill=False,      # remove background
                        edgecolor='red'
                    )
                )

        return ax

    def draw_points(self, points, ax=None):
        if ax is None:
            ax = self.draw_map()

        for point in points:
            x = point[X]
            y = self.y_max - point[Y]
            ax.add_patch(patches.Circle((x, y), 5, fill=True, color='yellow'))

        return ax

    def draw_roadmap(self, ax=None):
        if ax is None:
            ax = self.draw_map()

        pos = NODE_LOCATIONS
        for node, location in pos.items():
            pos.update({node: (location[X], self.y_max - location[Y])})

        nx.draw_networkx_nodes(self.roadmap, pos, ax=ax)
        nx.draw_networkx_edges(self.roadmap, pos, ax=ax)
        nx.draw_networkx_labels(self.roadmap, pos, ax=ax)

        for node, location in pos.items():
            pos.update({node: (location[X], self.y_max - location[Y])})

if __name__ == '__main__':
    a = Scene()
    # path generation test
    # print(a.get_node_path('entrance', 'din_cor'))
    # start = np.array([100, 100])
    # end = np.array([600, 600])
    # points = a.get_full_path(start, end)
    # a.draw_points(points)

    # room detection test
    # print(a.which_room(NODE_LOCATIONS['entrance']))

    # drawing and locations test
    ax = a.draw_map()
    a.draw_roadmap(ax)
    plt.show()

