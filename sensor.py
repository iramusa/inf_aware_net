from __future__ import division
from __future__ import print_function

# imports
import numpy as np
import matplotlib.patches as patches

# self imports
import conversions

# constants
X = 0
Y = 1
THETA = 2
RHO = 0
PHI = 1

# sensor poses
SENSOR_POSES = {
    'cor_1': (875, 25, 160),
    'cor_2': (305, 25, 65),
    'cor_3': (305, 875, -65),
    'war_1': (875, 165, 135),
    'war_2': (455, 165, 45),
    'war_3': (455, 520, 0),
    'war_4': (455, 875, -45),
    'war_5': (875, 875, -135),
    'com_1': (270, 25, 150),
    'com_2': (25, 25, 60),
    'com_3': (270, 370, -135),
    'com_4': (25, 370, -45),
    'din_1': (270, 415, 150),
    'din_2': (25, 415, 60),
    'din_3': (270, 715, -135),
    'din_4': (25, 715, -45),
}


class Sensor(object):
    def __init__(self, building, pose, name, **kwargs):
        self.name = name
        self.position = pose[0:2]
        self.orientation = conversions.wrap2pi(np.deg2rad(pose[2]))
        self.switch = False
        self.building = building
        self.room = building.which_room(self.position)
        if self.room is None:
            raise Exception('Invalid sensor position!')

        self.bearing_error = kwargs.get('bearing_error', np.deg2rad(2.0))
        # self.bearing_error = kwargs.get('bearing_error', np.deg2rad(0.0))
        self.distance_error = kwargs.get('distance_error', 120.0)
        # self.distance_error = kwargs.get('distance_error', 0.0)
        self.p_d = kwargs.get('detection_rate', 0.99)
        self.p_fa = kwargs.get('false_alarm_rate', 0.0)
        self.min_range = kwargs.get('min_range', 40.0)
        self.max_range = kwargs.get('max_range', 1000.0)
        self.half_angle_of_view = kwargs.get('angle_of_view', np.deg2rad(70.0))/2

        self.detections = []
        self.target_ids = []
        self.target_colors = []
        self.target_behaviours = []
        self.motion_detected = False

    def clear_detections(self):
        self.detections = []
        self.target_ids = []
        self.target_behaviours = []
        self.motion_detected = False
        self.switch = False

    def pos2reading(self, points):
        pos_rel = points - self.position
        distance, bearing = conversions.cart2pol(pos_rel)
        rel_bearing = conversions.wrap2pi(bearing - self.orientation)

        return distance, rel_bearing

    def reading2pos(self, dist, bear):
        pol_point = np.array((dist, bear))
        pos_rel = conversions.pol2cart(pol_point)
        pos_abs = pos_rel + self.position
        return pos_abs

    def is_in_fov(self, points):
        """Are given points in the field of view of the sensor?

        :param points: array of points
        :return: array of booleans
        """
        distance, rel_bearing = self.pos2reading(points)

        target_in_room = self.building.is_point_in_room(points, self.room)
        target_in_range = np.logical_and(self.min_range <= distance, distance <= self.max_range)
        target_in_aov = np.abs(rel_bearing) <= self.half_angle_of_view

        target_in_fov = np.logical_and(np.logical_and(target_in_room, target_in_range), target_in_aov)
        # print('logic', target_in_room, target_in_range, target_in_aov)

        return target_in_fov

    def detect_motion(self, targets):
        self.switch = True
        points = np.array([employee.position for employee in targets])
        self.motion_detected = np.any(self.is_in_fov(points))

    def detect_targets(self, targets):
        self.switch = True
        points = np.array([employee.position for employee in targets])

        distance, rel_bearing = self.pos2reading(points)

        # print('dist', distance)
        # print('bear', rel_bearing)

        target_in_room = self.building.is_point_in_room(points, self.room)
        target_in_range = np.logical_and(self.min_range <= distance, distance <= self.max_range)
        target_in_aov = np.abs(rel_bearing) <= self.half_angle_of_view

        target_in_fov = np.logical_and(np.logical_and(target_in_room, target_in_range), target_in_aov)
        # print('logic', target_in_room, target_in_range, target_in_aov)

        rel_bearing = rel_bearing[target_in_fov]
        distance = distance[target_in_fov]

        bearing_measured = np.clip(
                            conversions.wrap2pi(
                                rel_bearing + self.bearing_error * np.random.randn(len(rel_bearing))),
                                   -self.half_angle_of_view, self.half_angle_of_view)

        distance_measured = np.clip(distance + self.distance_error * np.random.randn(len(rel_bearing)),
                                    self.min_range, self.max_range)

        detections = np.array((distance_measured, bearing_measured))

        self.motion_detected = np.any(target_in_fov)
        self.detections = np.reshape(detections, (2, len(distance_measured)))

    def identify_targets(self, targets):
        self.switch = True
        points = np.array([employee.position for employee in targets])
        colors = np.array([employee.color for employee in targets])

        distance, rel_bearing = self.pos2reading(points)

        target_in_room = self.building.is_point_in_room(points, self.room)
        target_in_range = np.logical_and(self.min_range <= distance, distance <= self.max_range)
        target_in_aov = np.abs(rel_bearing) <= self.half_angle_of_view

        target_in_fov = np.logical_and(np.logical_and(target_in_room, target_in_range), target_in_aov)

        rel_bearing = rel_bearing[target_in_fov]
        distance = distance[target_in_fov]

        bearing_measured = np.clip(
                            conversions.wrap2pi(
                                rel_bearing + self.bearing_error * np.random.randn(len(rel_bearing))),
                                   -self.half_angle_of_view, self.half_angle_of_view)

        distance_measured = np.clip(distance + self.distance_error * np.random.randn(len(rel_bearing)),
                                    self.min_range, self.max_range)

        detections = np.array((distance_measured, bearing_measured))

        self.motion_detected = np.any(target_in_fov)
        self.detections = np.reshape(detections, (2, len(distance_measured)))
        self.target_ids, = np.nonzero(target_in_fov)
        self.target_colors = colors[target_in_fov]

    def identify_behaviours(self, targets):
        self.switch = True
        points = np.array([employee.position for employee in targets])
        colors = np.array([employee.color for employee in targets])
        behaviours = np.array([employee.behaviour for employee in targets])

        distance, rel_bearing = self.pos2reading(points)

        target_in_room = self.building.is_point_in_room(points, self.room)
        target_in_range = np.logical_and(self.min_range <= distance, distance <= self.max_range)
        target_in_aov = np.abs(rel_bearing) <= self.half_angle_of_view

        target_in_fov = np.logical_and(np.logical_and(target_in_room, target_in_range), target_in_aov)

        rel_bearing = rel_bearing[target_in_fov]
        distance = distance[target_in_fov]

        bearing_measured = np.clip(
                            conversions.wrap2pi(
                                rel_bearing + self.bearing_error * np.random.randn(len(rel_bearing))),
                                   -self.half_angle_of_view, self.half_angle_of_view)

        distance_measured = np.clip(distance + self.distance_error * np.random.randn(len(rel_bearing)),
                                    self.min_range, self.max_range)

        detections = np.array((distance_measured, bearing_measured))

        self.motion_detected = np.any(target_in_fov)
        self.detections = np.reshape(detections, (2, len(distance_measured)))
        self.target_ids, = np.nonzero(target_in_fov)
        self.target_colors = colors[target_in_fov]
        self.target_behaviours = behaviours[target_in_fov]

    def draw_sensor(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        x = self.position[X]
        y = self.building.y_max - self.position[Y]

        ax.add_patch(
            patches.Circle((x, y), 5, fill=True, color='grey'))

        if not self.switch:
            color = 'grey'
        elif self.motion_detected:
            color = 'red'
        else:
            color = 'yellow'

        ax.add_patch(
            patches.Wedge((x, y), self.min_range,
                          np.rad2deg(-conversions.wrap2pi(self.orientation+self.half_angle_of_view)),
                          np.rad2deg(-conversions.wrap2pi(self.orientation-self.half_angle_of_view)),
                          width=10,
                          alpha=0.3,
                          color=color
                          )
        )

        return ax

    def draw_detections(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        if not self.switch:
            return ax

        if len(self.detections) == 0:
            return ax

        n = self.detections.shape[1]
        detections = np.copy(self.detections)
        detections[PHI, :] += self.orientation

        points = np.repeat(self.position, n).reshape((2, n)) + conversions.pol2cart(detections)
        points[Y, :] = self.building.y_max - points[Y, :]

        xlim = ax.get_xlim()
        ylim = ax.get_ylim()

        if len(self.target_ids) > 0:
            for i in range(n):
                color = self.target_colors[i]
                ax.scatter(points[X, i], points[Y, i], c=color, edgecolor='face', marker='*', s=100)
                try:
                    text = self.target_behaviours[i]
                    ax.text(points[X, i], points[Y, i], text, color=color)
                except IndexError:
                    pass

        else:
            ax.scatter(points[X, :], points[Y, :], c='black', edgecolor='face', marker='*', s=100)

        ax.set_xlim(xlim)
        ax.set_ylim(ylim)

        return ax


