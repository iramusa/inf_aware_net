from __future__ import division
from __future__ import print_function

# self imports
import scene
import sensor
import target
import model
import pfilter
import record
import strategy

# imports
import matplotlib.pyplot as plt
import numpy as np
np.set_printoptions(precision=0, suppress=True)
import random
import cProfile


# constants
NAMES = ['Adam', 'Bertha', 'Cecil', 'David', 'Eva', 'Frank', 'George', 'Helen']
COLORS = ['blue', 'green', 'yellow', 'violet', 'red', 'pink', 'aquamarine', 'brown']


class Simulator(object):
    def __init__(self, dt=0.1, pure_physics=False, strategist_type='full_random'):
        self.t = 0.0
        self.dt = dt
        self.strat = strategy.Strategist(strategist_type, dt)
        self.strat_type = strategist_type
        self.pure_physics = pure_physics
        self.names = NAMES[:]
        self.colors = COLORS[:]
        self.building = scene.Scene()
        self.cameras = []
        self.camera_active = None
        self.employees = []
        self.filters = []
        for camera, pose in sensor.SENSOR_POSES.items():
            self.cameras.append(sensor.Sensor(self.building, pose, camera))

        self.strat.add_sensors(self.cameras)
        self.show_detections = True

        plt.ion()

    def run_step(self):
        self.camera_active = None
        for camera in self.cameras:
            camera.clear_detections()

        self.t += self.dt

        # change target behaviours
        for employee in self.employees:
            employee.check_behaviour(self.dt)

        # move targets
        for employee in self.employees:
            if employee.behaviour == target.B_FROZEN:
                continue

            des_point = employee.check_waypoint()
            new_pos = model.move_target(employee.behaviour, employee.position, des_point, self.dt)
            reset_cnt = 0
            while employee.behaviour != target.B_TRAVERSING and \
                            self.building.which_room(new_pos) != target.BEH2ROOM[employee.behaviour]:
                new_pos = model.move_target(employee.behaviour, employee.position, des_point, self.dt)
                # safety for lock up of an employee
                reset_cnt += 1
                if reset_cnt > 20:
                    # print('saving employee')
                    new_pos = employee.goal_destination

            employee.update_position(new_pos)

        if not self.pure_physics:
            self.run_filter()
            self.draw_all()
            plt.pause(0.1)

        # print('t:', self.t)

    def run_filter(self, perform_update=True):
        for pf in self.filters:
            pf.predict(self.dt)

        # sensor selection and update
        if perform_update:
            self.camera_active = self.strat.select_sensor()
            cam = self.cameras[self.camera_active]
            cam.identify_targets(self.employees)

            for target_id, pf in enumerate(self.filters):
                pf.update_id_detection(target_id, cam)
                pf.calculate_entropy_post()
                pf.resample()

        # bin_gain = np.sum([pf.inf_gained_bin() for pf in self.filters])
        # gaus_gain = np.sum([pf.inf_gained_gaus() for pf in self.filters])
        # print('inf gain (bin):', bin_gain)
        # print('inf gain (gaus):', gaus_gain)

    def add_employee(self, beh):
        name = self.names.pop(0)
        color = self.colors.pop(0)
        self.employees.append(target.Target(self.building, name, color, next_beh=beh))
        if not self.pure_physics:
            new_pf = pfilter.SingleTargetFilter(self.building, name, color)
            self.filters.append(new_pf)
            self.strat.add_filter(new_pf)

    def add_fixed_employees(self):
        # self.add_employee(target.B_COMP_WORK)
        # self.add_employee(target.B_COMP_WORK)
        # self.add_employee(target.B_COMP_WORK)
        # self.add_employee(target.B_COMP_WORK)
        self.add_employee(target.B_COMP_WORK)
        self.add_employee(target.B_WARE_WORK)
        # self.add_employee(target.B_WARE_WORK)
        # self.add_employee(target.B_WARE_WORK)
        # self.add_employee(target.B_WARE_WORK)
        # self.add_employee(target.B_REPAIR)
        self.add_employee(target.B_DINING)
        # self.add_employee(target.B_DINING)
        # self.employees[0].freeze(np.array((100, 100)))

    def remove_employee(self, idx):
        name = self.employees[idx].name
        color = self.employees[idx].color
        self.names.append(name)
        self.colors.append(color)
        self.employees.pop(idx)
        self.filters.pop(idx)
        print('{0} ({1}) left the building.'.format(name, color))

    def draw_time(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        x = 30
        y = 60

        ax.text(x, y, str(self.t))

        return ax

    def draw_all(self):
        ax = self.building.draw_map()
        # self.building.draw_roadmap(ax)

        for pf in self.filters:
            pf.draw_particles(ax)
            # print(pf)

        for employee in self.employees:
            employee.draw_target(ax)
            # employee.draw_target_path(ax)

        for camera in self.cameras:
            camera.draw_sensor(ax)
            if camera.switch and self.show_detections:
                camera.draw_detections(ax)

        self.draw_time(ax)


def hund_steps(sim):
    for i in range(100):
        sim.run_step()

if __name__ == '__main__':
    seed = 1
    np.random.seed(seed)
    random.seed(seed)
    # sim = Simulator(dt=0.6, pure_physics=False, strategist_type='inf_projection_gaus')
    sim = Simulator(dt=0.6, pure_physics=False, strategist_type='inf_projection_bin')
    # sim = Simulator(dt=1.5, pure_physics=False, strategist_type='in_fov_prop')
    sim.add_fixed_employees()
    g_rec = record.Record(seed, sim)

    # profiling
    # cProfile.run('hund_steps(sim)')

    for i in range(1000):
        sim.run_step()
        g_rec.add_timestep(sim)

    g_rec.save_file()
