from __future__ import division
from __future__ import print_function

# self imports
import measures

# imports
import numpy as np


STRATEGIES = [
    'full_random',
    'in_fov',
    'in_fov_prop',
    'inf_gain',
    'max_ent_decrease',
    'max_std_decrease',
]


class Strategist(object):
    def __init__(self, strat_type, dt):
        self.dt = dt
        self.strat_type = strat_type
        self.sensors = []
        self.pfs = []
        self.strategies_dict = {
            'full_random': self.full_random,
            'in_fov': self.mass_in_fov,
            'in_fov_prop': self.mass_in_fov_prop,
            'inf_gain_bin': self.inf_gain_bin,
            'inf_gain_gaus': self.inf_gain_gaus,
            'user': self.user_preferred_inf,
            'inf_projection_gaus': self.inf_projection_gaus,
            'inf_projection_bin': self.inf_projection_bin,
        }
        pass

    def add_sensors(self, sensors):
        self.sensors = sensors

    def add_all_filters(self, filters):
        self.pfs = filters

    def add_filter(self, pf):
        self.pfs.append(pf)

    def select_sensor(self):
        return self.strategies_dict[self.strat_type]()

    def full_random(self):
        return np.random.randint(0, len(self.sensors))

    def mass_in_fov(self):
        SWITCH_THRESH = 0.1
        indices = np.array(range(len(self.sensors)))

        masses = []
        for sensor in self.sensors:
            mass = 0
            for pf in self.pfs:
                mass += np.count_nonzero(sensor.is_in_fov(pf.pos_p))/pf.n_particles

            masses.append(mass)

        masses = np.array(masses)
        above_thresh = masses > SWITCH_THRESH

        if np.count_nonzero(above_thresh) == 0:
            return self.full_random()
        else:
            relevant_indices = indices[above_thresh]
            sensor_idx = np.random.choice(relevant_indices)
            return sensor_idx

    def mass_in_fov_prop(self):
        indices = np.array(range(len(self.sensors)))

        masses = []
        for sensor in self.sensors:
            mass = 0
            for pf in self.pfs:
                mass += np.count_nonzero(sensor.is_in_fov(pf.pos_p))/pf.n_particles

            masses.append(mass)

        masses = np.array(masses)
        if np.sum(masses) > 0:
            p = masses/np.sum(masses)
            sensor_idx = np.random.choice(indices, p=p)
            return sensor_idx
        else:
            return self.full_random()

    def inf_gain_gaus(self):
        expected_gains = []
        for sensor in self.sensors:
            gain = 0
            for pf in self.pfs:
                gain += pf.calculate_exp_ig_gaus(sensor)

            expected_gains.append(gain)

        # for i, gain in enumerate(expected_gains):
        #     expected_gains[i] = np.round(gain, 3)
        # print('max gain', max(expected_gains))
        # print('gains', expected_gains)
        expected_gains = np.array(expected_gains)

        if np.max(expected_gains) > 0:
            return np.argmax(expected_gains)
        else:
            return self.full_random()

    def inf_gain_bin(self):
        expected_gains = []
        for sensor in self.sensors:
            gain = 0
            for pf in self.pfs:
                gain += pf.calculate_exp_ig_bin(sensor)

            expected_gains.append(gain)

        # for i, gain in enumerate(expected_gains):
        #     expected_gains[i] = np.round(gain, 3)
        # print('max gain', max(expected_gains))
        # print('gains', expected_gains)
        expected_gains = np.array(expected_gains)

        if np.max(expected_gains) > 0:
            return np.argmax(expected_gains)
        else:
            return self.full_random()

    def user_preferred_inf(self):
        pass

    def inf_projection_gaus(self):
        expected_gains = []
        for sensor in self.sensors:
            gain = 0
            for pf in self.pfs:
                gain += pf.calculate_inf_projection_gaus(sensor, projection_time=self.dt)

            expected_gains.append(gain)

        # for i, gain in enumerate(expected_gains):
        #     expected_gains[i] = np.round(gain, 3)
        # print('max gain', max(expected_gains))
        # print('gains', expected_gains)
        expected_gains = np.array(expected_gains)

        return np.argmax(expected_gains)

    def inf_projection_bin(self):
        expected_gains = []
        for sensor in self.sensors:
            gain = 0
            for pf in self.pfs:
                gain += pf.calculate_inf_projection_bin(sensor, projection_time=self.dt)

            expected_gains.append(gain)

        # for i, gain in enumerate(expected_gains):
        #     expected_gains[i] = np.round(gain, 3)
        # print('max gain', max(expected_gains))
        # print('gains', expected_gains)
        expected_gains = np.array(expected_gains)

        return np.argmax(expected_gains)

    def discount_volatile(self):
        pass
