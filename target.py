from __future__ import division
from __future__ import print_function

# imports
import matplotlib.patches as patches
import numpy as np

# self imports
from scene import NODE_LOCATIONS
import scene

# constants
X = 0
Y = 1
# behaviours
B_COMP_WORK = 'comp_work'
B_WARE_WORK = 'ware_work'
B_REPAIR = 'repair'
B_DINING = 'dining'
B_TRAVERSING = 'traversing'
B_FROZEN = 'frozen'

BEH2IDX = {
    B_COMP_WORK: 0,
    B_WARE_WORK: 1,
    B_REPAIR: 2,
    B_DINING: 3,
    B_TRAVERSING: 4,
    B_FROZEN: 5
}
IDX2BEH = {}
for key, val in BEH2IDX.items():
    IDX2BEH.update({val: key})

# transition table for behaviours assuming time step dt
TRANS_DT = 0.02
TRANS_TABLE = np.array([
    [0.9998, 0.00005, 0.00005, 0.0001],  # comp work
    # [0.005, 0.98, 0.005, 0.01],  # comp work
    [0.00005, 0.9995, 0.00035, 0.0001],  # ware work
    [0.00005, 0.00035, 0.9995, 0.0001],  # repair
    [0.0002, 0.0002, 0.0002, 0.9994],    # dining
])
CUM_TRANS_TABLE = np.cumsum(TRANS_TABLE, axis=1)

# behaviour to room matching
BEH2ROOM = {
    B_COMP_WORK: scene.R_COMPUTER,
    B_WARE_WORK: scene.R_WAREHOUSE,
    B_REPAIR: scene.R_WAREHOUSE,
    B_DINING: scene.R_DINING
}

BEH2SPEED = {
    B_COMP_WORK: 2,
    B_WARE_WORK: 50,
    B_REPAIR: 8,
    B_DINING: 2,
    B_TRAVERSING: 150,
    B_FROZEN: 0
}

BEH2NOISE = {
    B_COMP_WORK: 2,
    B_WARE_WORK: 50,
    B_REPAIR: 4,
    B_DINING: 2,
    B_TRAVERSING: 15,
    B_FROZEN: 0
}

# position tolerance
TOL = 20


class Target(object):
    def __init__(self, scene, name, color, next_beh=None, dummy=False):
        self.name = name
        self.color = color
        self.building = scene
        self.position = NODE_LOCATIONS['entrance']
        self.behaviour = B_TRAVERSING
        if next_beh is None:
            self.next_behaviour = B_COMP_WORK
        else:
            self.next_behaviour = next_beh

        if dummy:
            self.goal_destination = []
            self.path = []
        else:
            self.goal_destination = self.building.random_point_in(BEH2ROOM[self.next_behaviour])
            self.path = self.building.get_full_path(self.position, self.goal_destination)

        self.cum_trans_mat_arch = {}

    def freeze(self, position):
        self.behaviour = B_FROZEN
        self.position = position
        self.path = []

    def check_waypoint(self):
        if self.behaviour == B_TRAVERSING:
            if np.allclose(self.position, self.path[0], atol=TOL):
                self.path.pop(0)

            try:
                return self.path[0]
            except IndexError:
                self.behaviour = self.next_behaviour
                return None

        return None

    def check_behaviour(self, dt):
        if self.behaviour in [B_TRAVERSING, B_FROZEN]:
            return
        else:
            roll = np.random.random()
            try:
                cum_tran_mat = self.cum_trans_mat_arch[dt]
            except KeyError:
                tran_mat_dt = np.linalg.matrix_power(TRANS_TABLE, int(np.round(dt/TRANS_DT)))
                cum_tran_mat = np.cumsum(tran_mat_dt, axis=1)
                self.cum_trans_mat_arch.update({dt: cum_tran_mat})

            new_beh_id = np.argmax(roll < cum_tran_mat[BEH2IDX[self.behaviour]])
            new_beh = IDX2BEH[new_beh_id]
            if self.behaviour != new_beh:
                self.change_behaviour(new_beh)

    def change_behaviour(self, new_behaviour):
        self.next_behaviour = new_behaviour
        self.behaviour = B_TRAVERSING
        self.goal_destination = self.building.random_point_in(BEH2ROOM[self.next_behaviour])
        self.path = self.building.get_full_path(self.position, self.goal_destination)

    def update_position(self, position):
        self.position = position

    def is_at_goal(self):
        return np.allclose(self.position, self.goal_destination, atol=TOL)

    def draw_target(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        x = self.position[X]
        y = self.building.y_max - self.position[Y]

        ax.add_patch(patches.Circle((x, y), 10, fill=True, facecolor=self.color, edgecolor='black'))
        ax.text(x, y, self.behaviour)

        return ax

    def draw_target_path(self, ax=None):
        if ax is None:
            ax = self.building.draw_map()

        for point in self.path:
            x = point[X]
            y = self.building.y_max - point[Y]
            ax.add_patch(patches.Circle((x, y), 10, fill=False, color=self.color))

        return ax
